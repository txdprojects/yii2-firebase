<?php

namespace txd\firebase;

use yii\base\Component;

/**
 * Firebase is a wrapper component for [[\Kreait\Firebase\Factory]].
 *
 * Example of usage:
 *
 * ```php
 * Yii::$app->firebase->getFactory()
 * ```
 *
 * @link https://firebase-php.readthedocs.io/en/5.1.1/setup.html
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class Firebase extends Component
{
	/**
	 * @var array The credentials configuration.
	 */
	public $credentials;

	/**
	 * @var \Kreait\Firebase\Factory The factory instance.
	 */
	private $_factory;


	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();

		$this->_factory = (new \Kreait\Firebase\Factory)->withServiceAccount($this->credentials);
	}

	/**
	 * Gets the instance of [[\Kreait\Firebase\Factory]].
	 *
	 * @return \Kreait\Firebase\Factory
	 */
	public function getFactory()
	{
		return $this->_factory;
	}
}
